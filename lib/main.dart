import 'package:adonis_flutter_interns/views/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:adonis_flutter_interns/views/signin.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sendy Interns',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
    );
  }
}