import 'package:adonis_flutter_interns/models/intern.dart';
import 'dart:core';
import 'package:http/http.dart' as http;
import 'dart:convert';

Future<List<Intern>> getInterns(String query) async {
  var url = 'http://127.0.0.1:3333/interns';
  final response = await http.get(url);
  if (response.statusCode == 200) {
    final List interns = json.decode(response.body);

    return interns.map((json) => Intern.fromJson(json)).where((intern) {
      final nameLower = intern.name.toLowerCase();
      final monthLower = intern.month.toLowerCase();
      final searchLower = query.toLowerCase();

      return nameLower.contains(searchLower) ||
          monthLower.contains(searchLower) ||
          (intern.year).toString() == searchLower
      ;
    }).toList();

  } else {
    throw Exception('Failed to fetch interns');
  }
}