import 'package:adonis_flutter_interns/models/stack.dart';

class Intern {
  final int id;
  final String name;
  final int year;
  final String month;
  final Stack stacks;
  Intern({this.id, this.name, this.year, this.month, this.stacks});
  factory Intern.fromJson(Map<String, dynamic> json) {
    return Intern(
      id: json['id'],
      name: json['name'],
      year: json['year'],
      month: json['month'],
      stacks: json['stacks']
    );
  }
}