class Stack {
  final String name;
  Stack({this.name});
  factory Stack.fromJson(Map<String, dynamic> json) {
    return Stack(
      name: json['name'],
    );
  }
}